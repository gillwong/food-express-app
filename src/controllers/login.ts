import { PrismaClient } from "@prisma/client";
import { Router } from "express";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

const loginRouter = Router();
const prisma = new PrismaClient();

type LoginRequestBody = {
  username: string;
  password: string;
};

loginRouter.post("/", async (request, response) => {
  const { username, password } = request.body as LoginRequestBody;
  const user = await prisma.user.findUnique({ where: { username } });
  const credsIsCorrect =
    user !== null && (await bcrypt.compare(password, user.password));

  if (!credsIsCorrect)
    return response.status(401).json({ error: "Invalid username or password" });

  const userToTokenize = { id: user.id, username: user.username };
  const token = jwt.sign(userToTokenize, process.env.SECRET!);

  return response.json({ token, username: user.username, name: user.name });
});

export default loginRouter;
