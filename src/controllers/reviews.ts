import { PrismaClient } from "@prisma/client";
import { Router } from "express";
import idParser from "../middlewares/id-parser";

const reviewsRouter = Router();
const prisma = new PrismaClient();

reviewsRouter.use("/:id", idParser);

reviewsRouter.get("/", async (request, response) => {
  const reviews = await prisma.review.findMany();
  return response.json(reviews);
});

reviewsRouter.get("/:id", async (request, response, next) => {
  const id = request.parsedId!;
  const review = await prisma.review.findUnique({ where: { id } });
  if (review === null)
    return response.status(404).json({ error: "Review not found" });
  return response.json(review);
});

reviewsRouter.post("/", async (request, response, next) => {
  try {
    const review = await prisma.review.create({ data: request.body });
    return response.status(201).json(review);
  } catch (error) {
    next(error);
  }
});

reviewsRouter.put("/:id", async (request, response, next) => {
  const id = request.parsedId!;
  try {
    const review = await prisma.review.update({
      where: { id },
      data: {
        title: request.body.title,
        body: request.body.body,
        rating: request.body.rating,
        images: request.body.images,
      },
    });
    return response.json(review);
  } catch (error) {
    next(error);
  }
});

reviewsRouter.delete("/:id", async (request, response) => {
  const id = request.parsedId!;
  await prisma.review.delete({ where: { id } });
  return response.status(204).end();
});

export default reviewsRouter;
