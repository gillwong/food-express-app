import { PrismaClient } from "@prisma/client";
import { Router } from "express";
import bcrypt from "bcrypt";
import idParser from "../middlewares/id-parser";

type CreateUserRequestBody = {
  username: string;
  password: string;
  name: string;
};

const usersRouter = Router();
const prisma = new PrismaClient();

usersRouter.use("/:id", idParser);

usersRouter.get("/", async (request, response) => {
  const users = await prisma.user.findMany({
    select: {
      id: true,
      username: true,
      name: true,
      _count: { select: { reviews: true } },
    },
  });
  return response.json(users);
});

usersRouter.get("/:id", async (request, response, next) => {
  const id = request.parsedId!;
  const user = await prisma.user.findUnique({
    where: { id },
    select: { id: true, username: true, name: true, reviews: true },
  });
  if (user === null)
    return response.status(404).json({ error: "Place not found" });
  return response.json(user);
});

usersRouter.post("/", async (request, response, next) => {
  const reqBody = request.body as CreateUserRequestBody;
  try {
    const passwordHash = await bcrypt.hash(reqBody.password, 16);
    const user = await prisma.user.create({
      data: {
        username: reqBody.username,
        name: reqBody.name,
        password: passwordHash,
      },
      select: { id: true, username: true, name: true },
    });
    return response.status(201).json(user);
  } catch (error) {
    next(error);
  }
});

usersRouter.put("/:id", async (request, response, next) => {
  const id = request.parsedId!;
  try {
    const user = await prisma.user.update({
      where: { id },
      data: { name: request.body.name },
      select: {
        id: true,
        username: true,
        name: true,
        _count: { select: { reviews: true } },
      },
    });
    return response.json(user);
  } catch (error) {
    next(error);
  }
});

usersRouter.delete("/:id", async (request, response) => {
  const id = request.parsedId!;
  await prisma.user.delete({ where: { id } });
  return response.status(204).end();
});

export default usersRouter;
