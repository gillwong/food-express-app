import { Router, request } from "express";
import { PrismaClient } from "@prisma/client";
import idParser from "../middlewares/id-parser";

const placesRouter = Router();
const prisma = new PrismaClient();

placesRouter.use("/:id", idParser);

placesRouter.get("/", async (request, response) => {
  const places = await prisma.place.findMany({
    select: {
      id: true,
      name: true,
      description: true,
      image: true,
      reviews: { select: { rating: true } },
    },
  });
  return response.json(places);
});

placesRouter.get("/:id", async (request, response, next) => {
  const id = request.parsedId!;
  const place = await prisma.place.findUnique({
    where: { id },
    select: {
      id: true,
      name: true,
      description: true,
      image: true,
      reviews: true,
    },
  });
  if (place === null)
    return response.status(404).json({ error: "Place not found" });
  return response.json(place);
});

placesRouter.post("/", async (request, response, next) => {
  try {
    const place = await prisma.place.create({ data: request.body });
    return response.status(201).json(place);
  } catch (error) {
    next(error);
  }
});

placesRouter.put("/:id", async (request, response, next) => {
  const id = request.parsedId!;
  try {
    const place = await prisma.place.update({
      where: { id },
      data: {
        name: request.body.name,
        description: request.body.description,
        image: request.body.image,
      },
    });
    return response.json(place);
  } catch (error) {
    next(error);
  }
});

placesRouter.delete("/:id", async (request, response) => {
  const id = request.parsedId!;
  await prisma.place.delete({ where: { id } });
  return response.status(204).end();
});

export default placesRouter;
