import { PrismaClientValidationError } from "@prisma/client/runtime/library";
import type { ErrorRequestHandler } from "express";

const errorHandler: ErrorRequestHandler = function (
  error,
  request,
  response,
  next
) {
  if (error instanceof PrismaClientValidationError) {
    return response.status(400).json({ error: "Invalid request body shape" });
  }
  next(error);
};

export default errorHandler;
