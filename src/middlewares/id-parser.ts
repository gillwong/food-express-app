import type { RequestHandler, Request, Response, NextFunction } from "express";

const idParser: RequestHandler = function (request, response, next) {
  const id = parseInt(request.params.id);
  if (isNaN(id)) {
    throw new Error("Invalid id request parameter");
  }
  request.parsedId = id;
  next();
};

export default idParser;
