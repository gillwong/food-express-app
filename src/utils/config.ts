import { PrismaClient } from "@prisma/client";
import dotenv from "dotenv";

dotenv.config();

export const PORT = process.env.PORT ?? "8000";
