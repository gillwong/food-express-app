import express from "express";
import cors from "cors";
import morgan from "morgan";
import { PORT } from "./src/utils/config";
import reviewsRouter from "./src/controllers/reviews";
import usersRouter from "./src/controllers/users";
import errorHandler from "./src/middlewares/error-handler";
import placesRouter from "./src/controllers/places";
import loginRouter from "./src/controllers/login";

const app = express();

app.use(cors());
app.use(morgan("tiny"));
app.use(express.json());

app.use("/api/login", loginRouter);
app.use("/api/reviews", reviewsRouter);
app.use("/api/users", usersRouter);
app.use("/api/places", placesRouter);

app.use(errorHandler);

app.listen(PORT, () => {
  console.log(`Server running at port ${PORT}`);
});
